;
FacebookFamilyFeatures = (function () {
    return {
        liked: {
            id: 1,
            enabled: true,
            description: 'Hide posts of your friends liking a page or photo.',
            pattern: {
                en: / like[sd]? /i,
                nl: / vind.* leuk/i,
                de: / gefällt /i,
                uk: / вподоба/i
            }
        },
        commented: {
            id: 2,
            enabled: true,
            description: 'Hide posts of your friends commenting on another post.',
            pattern: {
                en: / commented on /i,
                nl: /hierop gereageerd/i,
                de: / das kommentiert/i,
                uk: / прокоментува/i
            }
        },
        replied: {
            id: 3,
            enabled: true,
            description: 'Hide posts of your friends replying to a comment on another post.',
            pattern: {
                en: / replied to /i,
                nl: / gereageerd op een /i,
                de: / hat .* geantwortet/i,
                //uk: //i
            }
        },
        event: {
            id: 4,
            enabled: true,
            description: 'Hide posts of your friends going to an event.',
            pattern: {
                en: / going to an event/i,
                nl: / naar een evenement/i,
                de: / einer veranstaltung teil/i,
                uk: / на подію/i
            }
        },
        friends: {
            id: 5,
            enabled: true,
            description: 'Hide posts of your friends befriending other people.',
            pattern: {
                en: / are now friends/i,
                nl: / vrienden/i,
                de: / sind jetzt freunde/i,
                uk: / тепер друзі/i
            }
        },
        play: {
            id: 6,
            enabled: true,
            description: 'Hide posts of your friends playing a game.',
            pattern: {
                en: / play/i,
                nl: / spe+l(en)?t?/i,
                de: / spiel(en)?t?/i,
                //uk: //i
            }
        },
        movie: {
            id: 7,
            enabled: true,
            description: 'Hide posts of your friends watching a movie.',
            pattern: {
                en: / watching /i,
                nl: / kijk.*\b /i,
                de: / schaut /i,
                //uk: //i
            }
        },
        shared: {
            id: 8,
            enabled: false,
            description: 'Hide posts that your friends shared on their timeline.',
            pattern: {
                en: / shared /i,
                nl: / gedeeld/i,
                de: / geteilt/i,
                uk: / пошири.* /i
            }
        },
        via: {
            id: 9,
            enabled: true,
            description: 'Hide posts that your friends shared via an app.',
            pattern: {
                en: / via /i,
                nl: / via /i,
                de: / via /i,
                uk: / через /i
            }
        },
        suggested: {
            id: 10,
            enabled: true,
            description: 'Hide posts that are suggested by Facebook.',
            pattern: {
                en: /suggested post/i,
                nl: /voorgesteld[e]?/i,
                de: /vorgeschlagener beitrag/i,
                uk: /(запропонований допис|пропоноване відео)/i
            }
        },
        tagged: {
            id: 11,
            enabled: false,
            description: 'Hide posts that your friends are tagged in.',
            pattern: {
                en: / tagged in /i,
                nl: / getagd in /i,
                de: / in .* markiert/i,
                uk: / позначен.* (у|на)?/i
            }
        },
        timeline: {
            id: 12,
            enabled: false,
            description: 'Hide posts that your friends wrote on someone\'s timeline.',
            pattern: {
                en: / wrote on .* timeline/i,
                nl: / iets op de tijdlijn van /i,
                de: / in .* chronik geschrieben/i,
                //uk: //i
            }
        },
        photo: {
            id: 13,
            enabled: false,
            description: 'Hide posts of your friends adding new photos.',
            pattern: {
                en: / added .* new photo[s]?/i,
                nl: / nieuwe foto.{0,2} toegevoegd/i,
                de: / hat .* hinzugefügt/i,
                uk: / додає (світлину)?/i
            }
        },
        cover: {
            id: 14,
            enabled: false,
            description: 'Hide posts of your friends\' new cover photos.',
            pattern: {
                en: / updated /i,
                nl: / omslagfoto bijgewerkt/i,
                de: / titelbild aktualisiert/i,
                uk: / обкладинку/i
            }
        },
        profile: {
            id: 15,
            enabled: false,
            description: 'Hide posts of your friends\' new profile photos.',
            pattern: {
                en: / changed /i,
                nl: / heeft .* profielfoto gewijzigd/i,
                de: / profilbild geändert/i,
                uk: / основну світлину/i
            }
        }
    };
}());
