;
(function () {

    var languages = {
        en: 'English',
        nl: 'Nederlands',
        de: 'Deutsch',
        uk: 'Українська'
    };

    var locale = $('html').attr('lang');

    if (!languages[locale]) {
        return;
    }

    var patterns = [];

    $.each(Object.keys(FacebookFamilyFeatures), function (index, name) {
        var feature = FacebookFamilyFeatures[name];

        if (feature.enabled) {
            patterns.push(feature.pattern[locale] || feature.pattern['en']);
        }
    });

    function nodeInserted(event) {
        var $target = $(event.target);

        $.each(patterns, function (index, pattern) {
            if (isValidTitle($target, pattern)) {
                hide($target);
                return true;
            }
        });
    }

    function documentNodeInserted(event) {
        var content = document.getElementById('contentArea');

        if (content) {
            content.addEventListener('DOMNodeInserted', nodeInserted);
            document.removeEventListener('DOMNodeInserted', documentNodeInserted);
        }

        var $target = $(event.target);

        if (!isValidParent($target)) {
            return;
        }

        nodeInserted(event);
    }

    document.addEventListener('DOMNodeInserted', documentNodeInserted);

    function isValidTitle($target, pattern) {
        return pattern.test($target[0].innerText);
    }

    function isValidParent($target) {
        return $target.parents('#contentArea').length === 1;
    }

    function hide($target) {
        $target.css('display') === 'none' || $target.css({
            'display': 'none'
        });
    }
}());
