;
(function (browser) {

    var $form = $('#form');
    var $change_message = $('#change-message');

    var features = {};
    var settings = browser.storage.sync;

    $.each(FacebookFamilyFeatures, function (index, feature) {
        features[feature.id] = feature;

        $form.append('<input type="checkbox" id="' + feature.id + '"> ' + feature.description + '<br />');

        var setting = {};
        setting[feature.id] = feature.enabled;

        settings.get(setting, function (setting) {
            this.enabled = !!setting[this.id];

            if (this.enabled) {
                $('#' + this.id).attr('checked', true);
            }
        }.bind(feature));
    });

    var $checkboxes = $(':checkbox');

    $checkboxes.on('change', function () {
        var enabled = !!this.checked;
        var feature = features[this.id];

        if (feature.enabled === enabled) {
            return;
        }

        feature.enabled = enabled;
        $change_message.css('display', 'block');

        var setting = {};
        setting[feature.id] = feature.enabled;
        settings.set(setting);
    });

    $('#select-all').on('click', toggleAll.bind(this, true));
    $('#deselect-all').on('click', toggleAll.bind(this, false));

    function toggleAll(enabled) {
        $checkboxes.prop('checked', enabled);
        $checkboxes.trigger('change');
    }

}(chrome || opera));
