# **Facebook Family** #
This extension for [Google Chrome](http://www.google.com/chrome/) and [Opera](http://www.opera.com/computer/) cleans up your timeline on Facebook.

## How does it work? ##
Every time a new post is loaded in by Facebook, Facebook Family will decide whether it is worth displaying, based on your settings.

The following settings can be toggled *on* or *off*:

* Hide posts of your friends liking a page or photo

* Hide posts of your friends commenting on another post

* Hide posts of your friends replying to a comment on another post

* Hide posts of your friends going to an event

* Hide posts of your friends befriending other people

* Hide posts of your friends playing a game

* Hide posts of your friends watching a movie

* Hide posts that your friends shared on their timeline

* Hide posts that your friends shared via an app

* Hide posts that are suggested by Facebook

* Hide posts that your friends are tagged in

* Hide posts that your friends wrote on someone's timeline

* Hide posts of your friends adding new photos

* Hide posts of your friends' new cover photos

* Hide posts of your friends' new profile photos

## Supported Languages ##
Language support is growing, but we already support the following languages:

* English

* Dutch

* German

* Ukrainian (Partially)

## Developer Instructions ##
This extension is ready to run locally, with a very simple setup process.

1. Download the source code or checkout the repository to your computer.

2. Enable developer extension support at *[chrome://extensions](chrome://extensions)* or *[opera://extensions](opera://extensions)*.

3. Hit the button to load an unpacked extension and select the root directory of the project.